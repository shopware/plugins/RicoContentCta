<?php

/**
 * MIT License
 *
 * Copyright (c) 2020 PSVneo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

namespace RicoContentCta\Subscriber;

use Enlight\Event\SubscriberInterface;
use Enlight_Controller_ActionEventArgs;
use Enlight_Template_Manager;

class TemplateSubscriber implements SubscriberInterface
{
    /**
     * @var string
     */
    private $pluginDirectory;

    /**
     * @var Enlight_Template_Manager
     */
    private $templateManager;

    public function __construct(string $pluginDirectory, Enlight_Template_Manager $templateManager)
    {
        $this->pluginDirectory = $pluginDirectory;
        $this->templateManager = $templateManager;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            'Enlight_Controller_Action_PreDispatch_Backend_RicoContentCta' => 'templateDispatch',
            'Enlight_Controller_Action_PostDispatch_Frontend' => ['templateDispatch', 999999],
            'Enlight_Controller_Action_PostDispatch_Widgets_Emotion' => ['templateDispatch', 999999],
            'Enlight_Controller_Action_PostDispatchSecure_Widgets_Emotion' => ['templateDispatch', 999999],
            'Enlight_Controller_Action_PostDispatch_Widgets_EmotionFix' => ['templateDispatch', 999999],
        ];
    }

    public function templateDispatch(Enlight_Controller_ActionEventArgs $eventArgs): void
    {
        $templates = $this->templateManager->getTemplateDir();
        array_unshift($templates, $this->pluginDirectory . '/Resources/views');
        $this->templateManager->setTemplateDir($templates);
    }
}
