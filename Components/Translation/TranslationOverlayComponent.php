<?php

/**
 * MIT License
 *
 * Copyright (c) 2020 PSVneo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

namespace RicoContentCta\Components\Translation;

use InvalidArgumentException;
use Shopware\Components\Model\ModelEntity;
use Shopware_Components_Translation as TranslationComponent;

class TranslationOverlayComponent
{
    /**
     * @var TranslationComponent
     */
    private $translationComponent;

    public function __construct(TranslationComponent $translation)
    {
        $this->translationComponent = $translation;
    }

    public function translate(int $shopId, array $originalRecords, string $type): array
    {
        $ids = $this->collectRecordIds($originalRecords);
        $translations = $this->translationComponent->readBatch($shopId, $type, $ids);
        $translatedRecords = [];
        foreach ($originalRecords as $originalRecord) {
            foreach ($translations as $translation) {
                if ((int) $translation['objectkey'] !== (int) $originalRecord->getId()) {
                    continue;
                }
                $translatedRecords[] = $originalRecord->fromArray((array) $translation['objectdata']);
            }
        }

        return $translatedRecords;
    }

    private function collectRecordIds(array $originalRecords): array
    {
        $ids = [];
        foreach ($originalRecords as $originalRecord) {
            if (!$originalRecord instanceof ModelEntity) {
                throw new InvalidArgumentException("The given object '" . get_class($originalRecord) . "' does not extend from " . ModelEntity::class);
            }
            $ids[] = $originalRecord->getId();
        }

        return $ids;
    }
}
