<?php

/**
 * MIT License
 *
 * Copyright (c) 2020 PSVneo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

namespace RicoContentCta\Models;

use Doctrine\ORM\Mapping as ORM;
use Shopware\Components\Model\ModelEntity;
use Shopware\Models\Media\Media;

/**
 * @ORM\Entity(repositoryClass="RicoContentCta\Repository\ContentCtaRepository")
 * @ORM\Table(name="rico_content_cta")
 */
class ContentCtaModel extends ModelEntity
{
    public const TRANSLATION_KEY = 'content_cta';

    /**
     * @ORM\Column(name="image", type="integer", nullable=true)
     */
    protected $mediaId = 0;

    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column()
     */
    private $name = '';

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $imageLeft = true;

    /**
     * @var Media|null
     *
     * @ORM\ManyToOne(targetEntity="Shopware\Models\Media\Media")
     * @ORM\JoinColumn(name="image", referencedColumnName="id", nullable=true)
     */
    private $image;

    /**
     * @var string
     * @ORM\Column()
     */
    private $upperText = '';

    /**
     * @var string
     * @ORM\Column()
     */
    private $lowerText = '';

    /**
     * @var string
     * @ORM\Column()
     */
    private $buttonText = '';

    /**
     * @var string
     * @ORM\Column()
     */
    private $linkTarget = '';

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return bool
     */
    public function isImageLeft()
    {
        return $this->imageLeft;
    }

    /**
     * @param bool $imageLeft
     */
    public function setImageLeft($imageLeft)
    {
        $this->imageLeft = $imageLeft;
    }

    /**
     * @return Media
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param Media $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return string
     */
    public function getUpperText()
    {
        return $this->upperText;
    }

    /**
     * @param string $upperText
     */
    public function setUpperText($upperText)
    {
        $this->upperText = $upperText;
    }

    /**
     * @return string
     */
    public function getLowerText()
    {
        return $this->lowerText;
    }

    /**
     * @param string $lowerText
     */
    public function setLowerText($lowerText)
    {
        $this->lowerText = $lowerText;
    }

    /**
     * @return string
     */
    public function getButtonText()
    {
        return $this->buttonText;
    }

    /**
     * @param string $buttonText
     */
    public function setButtonText($buttonText)
    {
        $this->buttonText = $buttonText;
    }

    /**
     * @return string
     */
    public function getLinkTarget()
    {
        return $this->linkTarget;
    }

    /**
     * @param string $linkTarget
     */
    public function setLinkTarget($linkTarget)
    {
        $this->linkTarget = $linkTarget;
    }

    public function toArray(): array
    {
        return get_object_vars($this);
    }
}
