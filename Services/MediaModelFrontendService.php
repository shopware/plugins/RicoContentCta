<?php

/**
 * MIT License
 *
 * Copyright (c) 2020 PSVneo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

namespace RicoContentCta\Services;

use Shopware\Bundle\StoreFrontBundle\Service\Core\ContextService;
use Shopware\Bundle\StoreFrontBundle\Service\Core\MediaService;
use Shopware\Bundle\StoreFrontBundle\Struct\Media;
use Shopware\Bundle\StoreFrontBundle\Struct\ShopContextInterface;
use Shopware\Components\Compatibility\LegacyStructConverter;

/**
 * This class is used to convert \Shopware\Models\Media\Media to article like media struct used by the frontend.
 * This "struct" will contain any data known by the $sArticles.image property.
 */
class MediaModelFrontendService implements MediaModelFrontendServiceInterface
{
    /**
     * @var ContextService
     */
    protected $contextService;

    /**
     * @var MediaService
     */
    protected $mediaService;

    /**
     * @var LegacyStructConverter
     */
    protected $legacyStructConverter;

    public function __construct(
        ContextService $contextService,
        MediaService $mediaService,
        LegacyStructConverter $legacyStructConverter
    ) {
        $this->contextService = $contextService;
        $this->mediaService = $mediaService;
        $this->legacyStructConverter = $legacyStructConverter;
    }

    public function convert(int $mediaId): array
    {
        return $this->legacyStructConverter->convertMediaStruct(
            $this->getMediaStruct($mediaId)
        );
    }

    /**
     * @param int[] $mediaIds
     */
    public function convertMultiple(array $mediaIds): array
    {
        $result = [];
        foreach ($this->getMultipleMediaStructs($mediaIds) as $media) {
            $result[] = $this->legacyStructConverter->convertMediaStruct($media);
        }

        return $result;
    }

    protected function getMediaStruct(int $mediaId): Media
    {
        return $this->mediaService->get($mediaId, $this->getContext());
    }

    /**
     * @param int[] $mediaIds
     *
     * @return Media[]
     */
    protected function getMultipleMediaStructs(array $mediaIds): array
    {
        return $this->mediaService->getList($mediaIds, $this->getContext());
    }

    protected function getContext(): ShopContextInterface
    {
        return $this->contextService->getShopContext();
    }
}
