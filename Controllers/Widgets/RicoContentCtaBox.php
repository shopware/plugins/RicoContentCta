<?php

/**
 * MIT License
 *
 * Copyright (c) 2020 PSVneo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

use RicoContentCta\Components\Translation\TranslationOverlayComponent;
use RicoContentCta\Models\ContentCtaModel;

class Shopware_Controllers_Widgets_RicoContentCtaBox extends Enlight_Controller_Action
{
    public function indexAction()
    {
        if (!$this->request->has('cta_selection')) {
            return;
        }
        $ctas = $this->extractCTAsOfRequest();
        if (!Shopware()->Shop()->getDefault()) {
            $ctas = $this->getTranslationOverlayComponent()->translate(
                Shopware()->Shop()->getId(),
                $ctas,
                ContentCtaModel::TRANSLATION_KEY
            );
        }
        $ctas = $this->enrichImageData($ctas);
        $pluginDir = $this->container->getParameter('rico_content_cta.plugin_dir');
        $this->view->addTemplateDir($pluginDir . '/Resources/views');
        $this->view->loadTemplate($pluginDir . '/Resources/views/widgets/rico_content_cta_box/index.tpl');
        $this->view->assign('content_cta', $ctas, true);
    }

    /**
     * @return ContentCtaModel[]
     */
    protected function extractCTAsOfRequest(): array
    {
        $result = [];
        foreach (explode('|', $this->request->get('cta_selection')) as $cta) {
            if (empty($cta)) {
                continue;
            }

            $cta = $this->getModelManager()->getRepository(ContentCtaModel::class)->find($cta);
            if (!$cta instanceof ContentCtaModel) {
                continue;
            }
            $result[] = $cta;
        }

        return $result;
    }

    /**
     * @param \RicoContentCta\Models\ContentCtaModel[] $ctas
     */
    protected function enrichImageData(array $ctas): array
    {
        $mediaService = $this->getMediaModelFrontendService();
        $result = [];
        foreach ($ctas as $cta) {
            $data = $cta->toArray();
            $data['media'] = $data['image'];
            $data['image'] = $data['image'] instanceof \Shopware\Models\Media\Media ?
                $mediaService->convert($data['image']->getId()) : null;
            $result[] = $data;
        }

        return $result;
    }

    protected function getTranslationOverlayComponent(): TranslationOverlayComponent
    {
        /** @var TranslationOverlayComponent $component */
        $component = $this->get('rico_content_cta.service.components.translation.translation_overlay_component');

        return $component;
    }

    protected function getMediaModelFrontendService(): \RicoContentCta\Services\MediaModelFrontendServiceInterface
    {
        /** @var \RicoContentCta\Services\MediaModelFrontendServiceInterface $service */
        $service = $this->get('rico_content_cta.service.media_model_frontend_service');

        return $service;
    }
}
