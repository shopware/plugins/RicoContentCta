<?php

/**
 * MIT License
 *
 * Copyright (c) 2020 PSVneo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

namespace RicoContentCta\Loader;

use Exception;
use RicoContentCta\Models\ContentCtaModel;
use Shopware\Bundle\AttributeBundle\Service\CrudService;

/**
 * Class AttributeLoader
 */
class AttributeLoader
{
    /**
     * @var CrudService
     */
    private $crudService;

    /**
     * AttributeLoader constructor.
     */
    public function __construct(CrudService $crudService)
    {
        $this->crudService = $crudService;
    }

    /**
     * entrypoint for attribute generation
     *
     * @throws Exception
     */
    public function load()
    {
        try {
            $this->loadArticle();
            $this->loadCategory();
            $this->loadEmotion();
            $this->loadStatic();
            $this->loadBlogArticle();
            $this->loadForms();
        } catch (Exception $exception) {
            throw new Exception($exception);
        }
    }

    /**
     * generates the attribute for article
     *
     * @throws Exception
     */
    private function loadArticle()
    {
        $this->crudService->update(
            's_articles_attributes',
            'content_cta_selection',
            'multi_selection',
            [
                'entity' => ContentCtaModel::class,
                'displayInBackend' => true,
                'label' => 'Content CTA',
            ],
            null,
            true
        );
    }

    /**
     * generates the attribute for category
     *
     * @throws Exception
     */
    private function loadCategory()
    {
        $this->crudService->update(
            's_categories_attributes',
            'content_cta_selection',
            'multi_selection',
            [
                'entity' => ContentCtaModel::class,
                'displayInBackend' => true,
                'label' => 'Content CTA',
            ],
            null,
            true
        );
    }

    /**
     * generates the attribute for emotion
     *
     * @throws Exception
     */
    private function loadEmotion()
    {
        $this->crudService->update(
            's_emotion_attributes',
            'content_cta_selection',
            'multi_selection',
            [
                'entity' => ContentCtaModel::class,
                'displayInBackend' => true,
                'label' => 'Content CTA',
            ],
            null,
            true
        );
    }

    /**
     * generates the attribute for static content pages
     *
     * @throws Exception
     */
    private function loadStatic()
    {
        $this->crudService->update(
            's_cms_static_attributes',
            'content_cta_selection',
            'multi_selection',
            [
                'entity' => ContentCtaModel::class,
                'displayInBackend' => true,
                'label' => 'Content CTA',
            ],
            null,
            true
        );
    }

    /**
     * generates the attribute for blog entries
     *
     * @throws Exception
     */
    private function loadBlogArticle()
    {
        $this->crudService->update(
            's_blog_attributes',
            'content_cta_selection',
            'multi_selection',
            [
                'entity' => ContentCtaModel::class,
                'displayInBackend' => true,
                'label' => 'Content CTA',
            ],
            null,
            true
        );
    }

    /**
     * generates the attribute for cms_forms
     */
    private function loadForms()
    {
        $this->crudService->update(
            's_cms_support_attributes',
            'content_cta_selection',
            'multi_selection',
            [
                'entity' => ContentCtaModel::class,
                'displayInBackend' => true,
                'label' => 'Content CTA',
            ],
            null,
            true
        );
    }
}
