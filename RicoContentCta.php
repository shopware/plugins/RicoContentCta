<?php

/**
 * MIT License
 *
 * Copyright (c) 2020 PSVneo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

namespace RicoContentCta;

use RicoContentCta\Loader\AttributeLoader;
use RicoContentCta\Loader\DatabaseLoader;
use Shopware\Bundle\AttributeBundle\Service\CrudService;
use Shopware\Components\Logger;
use Shopware\Components\Model\ModelManager;
use Shopware\Components\Plugin;
use Shopware\Components\Plugin\Context\InstallContext;
use Shopware\Components\Plugin\Context\UninstallContext;

class RicoContentCta extends Plugin
{
    public function install(InstallContext $context)
    {
        (new DatabaseLoader($this->getModelManager()))->install();
        try {
            (new AttributeLoader($this->getCrudService()))->load();
        } catch (\Exception $e) {
            $this->getPluginLogger()->error($e->getMessage());
        }
    }

    public function uninstall(UninstallContext $context)
    {
        if (!$context->keepUserData()) {
            (new DatabaseLoader($this->getModelManager()))->uninstall();
        }
        parent::uninstall($context);
    }

    protected function getModelManager(): ModelManager
    {
        /** @var ModelManager $modelManager */
        $modelManager = $this->container->get('models');

        return $modelManager;
    }

    protected function getCrudService(): CrudService
    {
        /** @var CrudService $service */
        $service = $this->container->get('shopware_attribute.crud_service');

        return $service;
    }

    protected function getPluginLogger(): Logger
    {
        /** @var Logger $logger */
        $logger = $this->container->get('pluginlogger');

        return $logger;
    }
}
