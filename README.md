[![pipeline status](https://git.riconnect.de/riconet-public/shopware/plugins/rico-content-cta/badges/master/pipeline.svg)](https://git.riconnect.de/riconet-public/shopware/plugins/rico-content-cta/commits/master)
# Shopware - CTA

Allows you to manage and output CTA elements.