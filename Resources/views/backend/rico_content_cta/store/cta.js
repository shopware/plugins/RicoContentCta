Ext.define('Shopware.apps.RicoContentCta.store.Cta', {
    extend: 'Shopware.store.Listing',
    model: 'Shopware.apps.RicoContentCta.model.Cta',
    configure: function () {
        return {
            controller: 'RicoContentCta'
        };
    }
});
