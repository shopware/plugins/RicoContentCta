// {namespace name=backend/rico_content_cta/view}
Ext.define('Shopware.apps.RicoContentCta.view.detail.Window', {
    extend: 'Shopware.window.Detail',
    alias: 'widget.campaign-detail-window',
    title: '{s name=detail/window/window_title}{/s}',
    height: 420,
    width: 900,
    configure: function () {
        return {
            translationKey: 'content_cta'
        };
    }
});
