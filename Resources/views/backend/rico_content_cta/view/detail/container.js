Ext.define('Shopware.apps.RicoContentCta.view.detail.Container', {
    extend: 'Shopware.model.Container',
    padding: 20,
    configure: function () {
        var me = this;
        return {
            controller: 'RicoContentCta',
            fieldSets: [
                {
                    title: '',
                    layout: 'fit',
                    fields: {
                        name: {
                            validator: me.validateEmptyString,
                            translatable: true
                        },
                        imageLeft: { translatable: true },
                        upperText: {
                            translatable: true
                        },
                        lowerText: {
                            translatable: true
                        },
                        linkTarget: { translatable: true },
                        mediaId: {
                            xtype: 'shopware-media-field',
                            removeBackground: true
                        }
                    }
                }
            ]
        };
    },
    validateEmptyString: function (value) {
        return !(!value || value === '');
    }
});
