// {namespace name=backend/rico_content_cta/view}
Ext.define('Shopware.apps.RicoContentCta.view.list.Window', {
    extend: 'Shopware.window.Listing',
    alias: 'widget.campaign-list-window',
    height: 450,
    title: '{s name=list/window/window_title}{/s}',
    configure: function () {
        return {
            listingGrid: 'Shopware.apps.RicoContentCta.view.list.Grid',
            listingStore: 'Shopware.apps.RicoContentCta.store.Cta'
        };
    }
});
