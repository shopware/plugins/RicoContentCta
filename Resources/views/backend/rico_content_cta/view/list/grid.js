Ext.define('Shopware.apps.RicoContentCta.view.list.Grid', {
    extend: 'Shopware.grid.Panel',
    alias: 'widget.campaign-listing-grid',
    region: 'center',
    configure: function () {
        return {
            detailWindow: 'Shopware.apps.RicoContentCta.view.detail.Window',
            columns: {
                name: {},
                linkTarget: {}
            }
        };
    }
});
