Ext.define('Shopware.apps.RicoContentCta.model.Cta', {
    extend: 'Shopware.data.Model',
    idProperty: 'id',
    fields: [
        { name: 'id', type: 'int', useNull: true },
        { name: 'name', type: 'string' },
        { name: 'imageLeft', type: 'boolean' },
        { name: 'upperText', type: 'string' },
        { name: 'lowerText', type: 'string' },
        { name: 'linkTarget', type: 'string' },
        { name: 'mediaId', type: 'int' }
    ],
    associations: [
        {
            relation: 'ManyToOne',
            field: 'mediaId',
            type: 'hasMany',
            model: 'Shopware.apps.Base.model.Media',
            name: 'getImage',
            associationKey: 'mediaAssociation'
        }
    ],
    configure: function () {
        return {
            controller: 'RicoContentCta',
            detail: 'Shopware.apps.RicoContentCta.view.detail.Container'
        };
    }
});
