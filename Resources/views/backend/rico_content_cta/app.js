Ext.define('Shopware.apps.RicoContentCta', {
    extend: 'Enlight.app.SubApplication',
    name: 'Shopware.apps.RicoContentCta',
    loadPath: '{url action=load}',
    bulkLoad: true,
    controllers: ['Main'],
    views: [
        'list.Window',
        'list.Grid',
        'detail.Container',
        'detail.Window'
    ],
    models: ['Cta'],
    stores: ['Cta'],
    launch: function () {
        return this.getController('Main').mainWindow;
    }
});
