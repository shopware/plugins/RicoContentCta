{extends file="parent:frontend/listing/listing.tpl"}

{block name="frontend_listing_listing_wrapper"}
    {$smarty.block.parent}
    {if !$hasEmotion && $sCategoryContent.attribute.content_cta_selection != ""}
        {action module="widgets" controller="RicoContentCtaBox" action="index" cta_selection=$sCategoryContent.attribute.content_cta_selection}
    {/if}
{/block}
