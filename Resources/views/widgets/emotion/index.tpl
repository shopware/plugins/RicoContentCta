{extends file="parent:widgets/emotion/index.tpl"}

{block name="widgets/emotion/index/emotion"}
    {$smarty.block.parent}
    {if $emotion && $emotion.attribute.content_cta_selection != ""}
        {action module="widgets" controller="RicoContentCtaBox" action="index" cta_selection=$emotion.attribute.content_cta_selection}
    {/if}
{/block}
