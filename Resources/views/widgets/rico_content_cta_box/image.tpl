{block name='rico_content_cta_image_wrapper'}
    {if $cta_box.image}
        <div class="image">
            {if $cta_box.image.thumbnails|@count > 0}
                {block name='rico_content_cta_picture'}
                    <picture>
                        <img srcset="{$cta_box.image.thumbnails[0].sourceSet}"
                             alt="{$cta_box.lowerText|strip_tags|trim}"
                             title="{$cta_box.lowerText|strip_tags|trim|truncate:160}" />
                    </picture>
                {/block}
            {else}
                {block name='rico_content_cta_image'}
                    <img src="{$cta_box.image.source}"
                         alt="{$cta_box.lowerText|strip_tags|trim}"
                         title="{$cta_box.lowerText|strip_tags|trim|truncate:160}" />
                {/block}
            {/if}
        </div>
    {/if}
{/block}
