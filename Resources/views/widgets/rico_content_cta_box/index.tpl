{block name='rico_content_cta_wrapper'}
    <div class="content-cta--wrapper">
        {if $content_cta}
            {foreach $content_cta as $cta_box}
                {block name='rico_content_cta_box'}
                    <div class="cta--box{if $cta_box.imageLeft} image--left{else} image--right{/if}">
                        <a href="{$cta_box.linkTarget}">
                            {if $cta_box.imageLeft}
                                {include file='widgets/rico_content_cta_box/image.tpl'}
                            {/if}
                            {block name='rico_content_cta_box_content'}
                                <div class="content">
                                    {block name='rico_content_cta_box_content_upper_text'}
                                        <p class="text upper-text{if $cta_box.upperText|strlen > 12} longer-text{/if}">
                                            {$cta_box.upperText|strip_tags}
                                        </p>
                                    {/block}
                                    {block name='rico_content_cta_box_content_lower_text'}
                                        <p class="text lower-text{if $cta_box.lowerText|strlen > 20} longer-text{/if}">
                                            {$cta_box.lowerText|strip_tags}
                                        </p>
                                    {/block}
                                    <button type="button">{s name="cta-box/buttonText"}{/s}</button>
                                </div>
                            {/block}
                            {if !$cta_box.imageLeft}
                                {include file='widgets/rico_content_cta_box/image.tpl'}
                            {/if}
                        </a>
                    </div>
                {/block}
            {/foreach}
        {/if}
    </div>
{/block}
